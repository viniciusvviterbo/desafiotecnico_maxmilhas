// Require objects.
var express  = require('express');
var aws      = require('aws-sdk');
var mongoose = require('mongoose');

var app      = express();
var queueUrl = "https://sqs.us-east-1.amazonaws.com/375720587151/filaSugestoes";
var receipt = "";
var txtSugestao = "Hello World!";
var txtId;

// Conecta ao MongoDB
mongoose.connect('mongodb://database:27017/application', {userNewUrlParser: true});

const Sugestao = require('./models/sugestao');

// Carrega as credenciais da AWS e tenta instanciar o objeto.
aws.config.loadFromPath('./config.json');

// Instancia SQS.
var sqs = new aws.SQS();

// Lista as sugestões armazenas
app.get('/', (req, res) => {
    Sugestao.find()
    .then(sugestoes => res.send(sugestoes))
    .catch(err => res.status(404).json({msg : 'Nenhuma sugestão encontrada'}));
});

// Envia uma mensagem.
app.get('/enviarMsg', function (req, res) {
    //Instancia txtSugestao com a sugestão enviada
    txtSugestao = req.query.sugestao;
    //Define os parâmetros a serem enviados
    var params = {
        MessageBody: txtSugestao,
        QueueUrl: queueUrl,
        DelaySeconds: 0
    };
    //Executa o envio da mensagem para a fila
    sqs.sendMessage(params, function(err, data) {
        if(err) {
            res.send(err);
        } 
        else {
            res.send("Mensagem enviada com sucesso.\nSugestão: " + txtSugestao + "\nId: " + data.MessageId);
        } 
    });
});

//Recebe uma mensagem.
app.get('/receberMsg', function (req, res) {
    //Define os parametros a serem enviados
    var params = {
        QueueUrl: queueUrl,
        VisibilityTimeout: 60
    };
    //Executa a captura da mensagem para processamento
    sqs.receiveMessage(params, function(err, data) {
        if(err) {
            res.send(err);
        } 
        else {
            txtSugestao = data.MessageBody;
            txtId = data.MessageId;
            receipt = data.Messages[0].ReceiptHandle;
            res.send("Sugestão: " + txtSugestao + "\n" + "Id: " + data.Messages[0].MessageId);
        } 
    });
    //Cria um objeto para armazenamento
    const novaSugestao = new Sugestao ({
        sugestao: txtSugestao,
        id: txtId
    });
    //Salva o objeto no banco
    novaSugestao.save();
});

// Deleta uma mensagem.
app.get('/deletarMsg', function (req, res) {
    //Define os parametros a serem enviados
    var params = {
        QueueUrl: queueUrl,
        ReceiptHandle: receipt
    };
    //Executa a exclusão da mensagem na fila
    sqs.deleteMessage(params, function(err, data) {
        if(err) {
            res.send(err);
        } 
        else {
            res.send(data);
        } 
    });
});

app.listen(3000, () => {
  console.log("Server running on port 3000");
});