const mongoose = require('mongoose');
const Schema   = mongoose.Schema

const SugestaoSchema = new Schema({
    sugestao: {
        type: String,
        required: true,
        default: ''
    },
    id: {
        type: Number,
        required: true
    }
});

module.exports = Sugestao = mongoose.model('sugestao', SugestaoSchema);