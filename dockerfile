FROM node:latest  
   
 RUN mkdir -p /usr/src/app  
 RUN npm install nodemon -g  
   
 WORKDIR /usr/src/app  
 
 COPY package*.json /usr/src/app/

 RUN npm install

 COPY . /usr/src/app

 EXPOSE 3000
   
 CMD ["npm", "start"]  